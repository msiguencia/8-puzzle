public class Board{
    
    //constructs a board from an N by N array
    public Board(int[][] board){}
    //board dimension N
    public int dimension(){return 0;}
    //the number of block out of place
    public int hamming(){return 0;}
    //the sum of the manhattan distances for each block
    public int manhattan(){return 0;}
    //is this board the goal board?
    public boolean isGoal(){return false;}
    //a board obtained by exchanging two adjacent blocks in the same row
    public Board twin(){return null;}
    //does this board equal the other board?
    public boolean equals(Object y){return false;}
    //all neighboring boards, boards resulting from a single move
    public Iterable<Board> neighbors(){return null;}
    //string representation of the board
    public String toString(){return null;}

}
