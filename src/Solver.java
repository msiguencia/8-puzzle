public class Solver{
    //find a solution to the initial board using A* algorithm
    public Solver(Board initial){}
    //is the initial board solvable?
    public boolean isSolvable(){return true;}
    //minimum number of moves to solve the initial board; -1 if no solution
    public int moves(){return 0;}
    //sequence of boards in the shortest solution; null if no solution
    public Iterable<Board> solution(){return null;}
    //solve a slider puzzle
    public static void main(String[] args){}
}
